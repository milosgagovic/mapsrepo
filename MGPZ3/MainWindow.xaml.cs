﻿using PZ1.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace MGPZ3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public NetworkModel NetworkModel;
        public Dictionary<string, Entity> allEntities;
        public Dictionary<GeometryModel3D, Entity> allModels = new Dictionary<GeometryModel3D, Entity>();
        public static List<GeometryModel3D> tempList = new List<GeometryModel3D>();
        public GeometryModel3D HitModel;
        public System.Windows.Point mouseposition;
        public Popup popup;

        public double minLat = 45.2325;
        public double maxLat = 45.277031;
        public double minLon = 19.793909;
        public double maxLon = 19.894459;
        public double MapWidth = 300;
        public double MapHeight = 150;

        private Point start = new Point();
        private Point diffOffset = new Point();

        private int zoomMax = 500;
        private int zoomMin = 0;
        private int zoomCurrent = 2;

        private bool isMouseDown;
        private double startAngleX;
        private double startAngleY;

        //camera const
        private double CameraPhi = Math.PI;      // 30 degrees
        private double CameraTheta = Math.PI;     // 30 degrees
        private double CameraR = 3.0;

        // The change in CameraPhi when you press up/down arrow.
        private const double CameraDPhi = 0.1;

        // The change in CameraTheta when you press left/right arrow.
        private const double CameraDTheta = 0.1;

        // The change in CameraR when you press + or -.
        private const double CameraDR = 0.1;

        private Model3DGroup MainModel3Dgroup = new Model3DGroup();
        public MainWindow()
        {
            InitializeComponent();

            NetworkModel = new NetworkModel();
            try
            {
                XmlSerializer serializer = new XmlSerializer(NetworkModel.GetType());
                using (FileStream reader = new FileStream("Geographic.xml", FileMode.Open))
                {
                    NetworkModel = (NetworkModel)serializer.Deserialize(reader);
                }
            }
            catch
            {
                MessageBox.Show("Error while reading Geographic.xml");
                this.Close();
            }

            allEntities = new Dictionary<string, Entity>();
            ////////////////////////////////////////////////////
            //Popunjavanje Recnika konvertovanim entitetima
            foreach (SubstationEntity ent in NetworkModel.Substations)
            {
                ToLatLon(ent.X, ent.Y, 34, out ent.Latitude, out ent.Longitude);
                if (ent.Latitude < maxLat && ent.Latitude > minLat && ent.Longitude > minLon && ent.Longitude < maxLon)
                {
                    allEntities[ent.Id] = ent;
                }
            }
            foreach (SwitchEntity ent in NetworkModel.Switches)
            {
                ToLatLon(ent.X, ent.Y, 34, out ent.Latitude, out ent.Longitude);
                if (ent.Latitude < maxLat && ent.Latitude > minLat && ent.Longitude > minLon && ent.Longitude < maxLon)
                {
                    allEntities[ent.Id] = ent;
                }
            }
            foreach (NodeEntity ent in NetworkModel.Nodes)
            {
                ToLatLon(ent.X, ent.Y, 34, out ent.Latitude, out ent.Longitude);
                if (ent.Latitude < maxLat && ent.Latitude > minLat && ent.Longitude > minLon && ent.Longitude < maxLon)
                {
                    allEntities[ent.Id] = ent;
                }
            }

            /////////////////////////////////////////////////////////////////
            //Crtanje entitija na mapu
            foreach (KeyValuePair<string, Entity> kvp in allEntities)
            {
                double x1, y1, x2, y2;
                x1 = ConvertRangeToXYZ(minLon, maxLon, -MapWidth / 2, MapWidth / 2, kvp.Value.Longitude);
                x2 = x1 + 0.5;
                x1 = x2 - 1;
                y1 = ConvertRangeToXYZ(minLat, maxLat, -MapHeight / 2, MapHeight / 2, kvp.Value.Latitude);
                y2 = y1;
                SolidColorBrush b;
                if (kvp.Value is SubstationEntity)
                {
                    b = Brushes.Green;

                }
                else if (kvp.Value is SwitchEntity)
                {
                    b = Brushes.Tomato;
                }
                else
                {
                    b = Brushes.Blue;
                }
                GeometryModel3D temp = GetModelElementCube(x1, -y1, x2, -y2, b);
                allModels[temp] = kvp.Value;
                MainModel3Dgroup.Children.Add(temp);
            }

            /////////////////////////////////////////////////////////////////
            //Crtanje ruta na mapu
            foreach (LineEntity ent in NetworkModel.Lines)
            {
                if (allEntities.ContainsKey(ent.FirstEnd) && allEntities.ContainsKey(ent.SecondEnd))
                {

                    double x1, y1, x2, y2, lon, lat;

                    for (int i = 0; i < ent.Vertices.Count - 1; i++)
                    {
                        ToLatLon(ent.Vertices[i].X, ent.Vertices[i].Y, 34, out lat, out lon);
                        x1 = ConvertRangeToXYZ(minLon, maxLon, -MapWidth / 2, MapWidth / 2, lon);
                        y1 = ConvertRangeToXYZ(minLat, maxLat, -MapHeight / 2, MapHeight / 2, lat);

                        ToLatLon(ent.Vertices[i + 1].X, ent.Vertices[i + 1].Y, 34, out lat, out lon);
                        x2 = ConvertRangeToXYZ(minLon, maxLon, -MapWidth / 2, MapWidth / 2, lon);
                        y2 = ConvertRangeToXYZ(minLat, maxLat, -MapHeight / 2, MapHeight / 2, lat);

                        GeometryModel3D temp = GetModelRoute(x1, -y1, x2, -y2);
                        allModels[temp] = ent;
                        tempList.Add(temp);
                        MainModel3Dgroup.Children.Add(temp);
                    }
                }
            }
            /////////////////////

            ModelVisual3D model_visual = new ModelVisual3D();
            model_visual.Content = MainModel3Dgroup;

            AllElementsCont.Children.Add(model_visual);

            // this.DataContext = this;
        }

        private GeometryModel3D GetModelElementCube(double x1, double y1, double x2, double y2, SolidColorBrush b)
        {
            MeshGeometry3D model = new MeshGeometry3D();

            double d = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
            double angle = Math.PI / 2;
            if (x1 != x2)
            {
                if (x2 - x1 < 0) // ako je razlika manja od nule obrnuti ugao
                {
                    angle = Math.Atan((y1 - y2) / (x2 - x1));
                    angle += Math.PI;
                    angle = -angle;
                }
                else
                {
                    angle = Math.Atan((y2 - y1) / (x2 - x1));
                }
            }

            double height = 2;

            model.Positions = new Point3DCollection();

            model.Positions.Add(new Point3D(0, 0, 0));
            model.Positions.Add(new Point3D(d, 0, 0));
            model.Positions.Add(new Point3D(0, height, 0));
            model.Positions.Add(new Point3D(d, height, 0));

            model.Positions.Add(new Point3D(0, 0, 1));
            model.Positions.Add(new Point3D(d, 0, 1));
            model.Positions.Add(new Point3D(0, height, 1));
            model.Positions.Add(new Point3D(d, height, 1));


            int[] vertices = new int[] { 2, 3, 1, 2, 1, 0, 7, 1, 3, 7, 5, 1, 6, 5, 7, 6, 4, 5, 6, 2, 4, 2, 0, 4, 2, 7, 3, 2, 6, 7, 0, 1, 5, 0, 5, 4 };
            model.TriangleIndices = new Int32Collection(vertices);

            DiffuseMaterial surface_material = new DiffuseMaterial(b);

            GeometryModel3D surface_model = new GeometryModel3D(model, surface_material);

            Transform3DGroup trGroup = new Transform3DGroup();

            angle = -180 * angle / Math.PI;
            Quaternion q = new Quaternion(new Vector3D(0, 1, 0), angle);
            QuaternionRotation3D qr3d = new QuaternionRotation3D(q);
            RotateTransform3D rt3d = new RotateTransform3D(qr3d, 0, 0, 0.5);
            trGroup.Children.Add(rt3d);
            trGroup.Children.Add(new TranslateTransform3D(x1, 0, y1));

            surface_model.Transform = trGroup;
            surface_model.BackMaterial = surface_material;

            return surface_model;
        }


        private double ConvertRangeToXYZ(double origStart, double origEnd, double newStart, double newEnd, double value)
        {
            double scale = (newEnd - newStart) / (origEnd - origStart);
            return (newStart + ((value - origStart) * scale));
        }
        public void ToLatLon(double utmX, double utmY, int zoneUTM, out double latitude, out double longitude)
        {
            bool isNorthHemisphere = true;

            var diflat = -0.00066286966871111111111111111111111111;
            var diflon = -0.0003868060578;

            var zone = zoneUTM;
            var c_sa = 6378137.000000;
            var c_sb = 6356752.314245;
            var e2 = Math.Pow((Math.Pow(c_sa, 2) - Math.Pow(c_sb, 2)), 0.5) / c_sb;
            var e2cuadrada = Math.Pow(e2, 2);
            var c = Math.Pow(c_sa, 2) / c_sb;
            var x = utmX - 500000;
            var y = isNorthHemisphere ? utmY : utmY - 10000000;

            var s = ((zone * 6.0) - 183.0);
            var lat = y / (c_sa * 0.9996);
            var v = (c / Math.Pow(1 + (e2cuadrada * Math.Pow(Math.Cos(lat), 2)), 0.5)) * 0.9996;
            var a = x / v;
            var a1 = Math.Sin(2 * lat);
            var a2 = a1 * Math.Pow((Math.Cos(lat)), 2);
            var j2 = lat + (a1 / 2.0);
            var j4 = ((3 * j2) + a2) / 4.0;
            var j6 = ((5 * j4) + Math.Pow(a2 * (Math.Cos(lat)), 2)) / 3.0;
            var alfa = (3.0 / 4.0) * e2cuadrada;
            var beta = (5.0 / 3.0) * Math.Pow(alfa, 2);
            var gama = (35.0 / 27.0) * Math.Pow(alfa, 3);
            var bm = 0.9996 * c * (lat - alfa * j2 + beta * j4 - gama * j6);
            var b = (y - bm) / v;
            var epsi = ((e2cuadrada * Math.Pow(a, 2)) / 2.0) * Math.Pow((Math.Cos(lat)), 2);
            var eps = a * (1 - (epsi / 3.0));
            var nab = (b * (1 - epsi)) + lat;
            var senoheps = (Math.Exp(eps) - Math.Exp(-eps)) / 2.0;
            var delt = Math.Atan(senoheps / (Math.Cos(nab)));
            var tao = Math.Atan(Math.Cos(delt) * Math.Tan(nab));

            longitude = ((delt * (180.0 / Math.PI)) + s) + diflon;
            latitude = ((lat + (1 + e2cuadrada * Math.Pow(Math.Cos(lat), 2) - (3.0 / 2.0) * e2cuadrada * Math.Sin(lat) * Math.Cos(lat) * (tao - lat)) * (tao - lat)) * (180.0 / Math.PI)) + diflat;
        }




        private GeometryModel3D GetModelRoute(double x1, double y1, double x2, double y2)
        {
            MeshGeometry3D model = new MeshGeometry3D();

            double d = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
            double angle = Math.PI / 2;
            double height = 0.2;
            if (x1 != x2)
            {
                if (x2 - x1 < 0) // ako je razlika manja od nule obrnuti ugao
                {
                    angle = Math.Atan((y1 - y2) / (x2 - x1));
                    angle += Math.PI;
                    angle = -angle;
                }
                else
                {
                    angle = Math.Atan((y2 - y1) / (x2 - x1));  // ako je razlika pozitivna
                }
            }

            model.Positions = new Point3DCollection();

            model.Positions.Add(new Point3D(0, 0, 0));
            model.Positions.Add(new Point3D(d, 0, 0));
            model.Positions.Add(new Point3D(0, height, 0));
            model.Positions.Add(new Point3D(d, height, 0));

            model.Positions.Add(new Point3D(0, 0, 1));
            model.Positions.Add(new Point3D(d, 0, 1));
            model.Positions.Add(new Point3D(0, height, 1));
            model.Positions.Add(new Point3D(d, height, 1));


            int[] vertices = new int[] { 2, 3, 1, 2, 1, 0, 7, 1, 3, 7, 5, 1, 6, 5, 7, 6, 4, 5, 6, 2, 4, 2, 0, 4, 2, 7, 3, 2, 6, 7, 0, 1, 5, 0, 5, 4 };
            model.TriangleIndices = new Int32Collection(vertices);

            DiffuseMaterial surface_material = new DiffuseMaterial(Brushes.Red);

            GeometryModel3D surface_model = new GeometryModel3D(model, surface_material);

            Transform3DGroup trGroup = new Transform3DGroup();

            angle = -180 * angle / Math.PI;

            Quaternion q = new Quaternion(new Vector3D(0, 1, 0), angle);
            QuaternionRotation3D qr3d = new QuaternionRotation3D(q);
            RotateTransform3D rt3d = new RotateTransform3D(qr3d, 0, 0, 0.5);
            trGroup.Children.Add(rt3d);
            trGroup.Children.Add(new TranslateTransform3D(x1, 0, y1));

            surface_model.Transform = trGroup;
            surface_model.BackMaterial = surface_material;  // da se materijal vidi odozdo

            return surface_model;
        }

        private void viewport1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            start = e.GetPosition(this);
            diffOffset.X = translation.OffsetX;
            diffOffset.Y = translation.OffsetZ;
            MainViwePort.CaptureMouse();

            mouseposition = e.GetPosition(MainViwePort);
            Point3D testpoint3D = new Point3D(mouseposition.X, 0, mouseposition.Y);
            Vector3D testdirection = new Vector3D(mouseposition.X, -10, mouseposition.Y);

            PointHitTestParameters pointparams =
                     new PointHitTestParameters(mouseposition);
            RayHitTestParameters rayparams =
                     new RayHitTestParameters(testpoint3D, testdirection);

            HitModel = null;
            VisualTreeHelper.HitTest(MainViwePort, null, HTResult, pointparams);
        }

        private HitTestResultBehavior
                HTResult(System.Windows.Media.HitTestResult rawresult)
        {

            RayHitTestResult rayResult = rawresult as RayHitTestResult;

            if (rayResult != null && rayResult.ModelHit != MapNoviSadModel)
            {
                bool gasit = false;

                if (popup != null)
                {
                    popup.IsOpen = false;
                }

                if (allModels.ContainsKey((GeometryModel3D)rayResult.ModelHit))
                {
                    Entity ent = allModels[(GeometryModel3D)rayResult.ModelHit];

                    TextBlock t = new TextBlock();
                    t.Width = 120;
                    t.Height = 60;
                    t.Foreground = Brushes.Black;
                    t.Background = Brushes.Wheat;
                    t.FontSize = 11;

                    string text = "";

                    if (ent is SubstationEntity)
                    {
                        text = " Substation\n ID: " + ent.Id + "\n Name: " + ent.Name + "\n";
                    }
                    else if (ent is SwitchEntity)
                    {
                        text = " Switch\n ID: " + ent.Id + "\n Name: " + ent.Name + "\n";
                    }
                    else if (ent is NodeEntity)
                    {
                        text = " Node\n ID: " + ent.Id + "\n Name: " + ent.Name + "\n";
                    }
                    else if (ent is LineEntity)
                    {
                        t.Height = 100;
                        LineEntity le = ent as LineEntity;
                        text = " Line\n ID: " + le.Id + "\n Name: " + le.Name + "\n ";
                    }


                    t.Text = text;

                    popup = new Popup();
                    popup.Child = t;
                    popup.PopupAnimation = PopupAnimation.Scroll;
                    popup.PlacementRectangle = new Rect(0, 0, t.Width + 10, t.Height + 10);
                    popup.PlacementTarget = MainViwePort;
                    popup.VerticalOffset = mouseposition.Y - 4;
                    popup.HorizontalOffset = mouseposition.X + 4;

                    popup.IsOpen = true;
                }

                if (!gasit)
                {
                    HitModel = null;
                }
            }
            return HitTestResultBehavior.Stop;
        }
        private void viewport1_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            MainViwePort.ReleaseMouseCapture();
        }

        private void viewport1_MouseMove(object sender, MouseEventArgs e)
        {
            if (MainViwePort.IsMouseCaptured)
            {
                if (popup != null)
                {
                    popup.IsOpen = false;
                }

                Point end = e.GetPosition(this);

                translation.OffsetX = diffOffset.X + (end.X - start.X) / (10 * scaling.ScaleX);
                translation.OffsetZ = diffOffset.Y + (end.Y - start.Y) / (10 * scaling.ScaleZ);

            }
            if (isMouseDown)
            {
                if (popup != null)
                {
                    popup.IsOpen = false;
                }

                Point end = e.GetPosition(this);

                double offsetX = end.X - start.X;
                double offsetY = end.Y - start.Y;
                double w = this.MapWidth;
                double h = this.MapHeight;
                double translateX = (offsetX * 100) / w;
                double translateY = -(offsetY * 100) / h;
                rotateX.Angle = -(this.startAngleX - (translateX));
                rotateZ.Angle = this.startAngleY - (translateY);
            }
        }

        private void viewport1_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (popup != null)
            {
                popup.IsOpen = false;
            }
            Point p = e.MouseDevice.GetPosition(this);
            double scaleX = 1;
            double scaleY = 1;
            double scaleZ = 1;
            double mapScale = MapScale.ScaleY;
            if (e.Delta > 0 && zoomCurrent < zoomMax)
            {
                scaleX = scaling.ScaleX + 0.15;
                scaleY = scaling.ScaleY + 0.15;
                scaleZ = scaling.ScaleZ + 0.15;
                zoomCurrent++;
                scaling.ScaleX = scaleX;
                scaling.ScaleY = scaleY;
                scaling.ScaleZ = scaleZ;
            }
            else if (e.Delta <= 0 && zoomCurrent > zoomMin)
            {
                scaleX = scaling.ScaleX - 0.2;
                scaleY = scaling.ScaleY - 0.2;
                scaleZ = scaling.ScaleZ - 0.2;
                zoomCurrent--;
                scaling.ScaleX = scaleX;
                scaling.ScaleY = scaleY;
                scaling.ScaleZ = scaleZ;
            }
            MapScale.ScaleY = mapScale;
        }

        private void viewport1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle)
            {
                isMouseDown = true;
                start = e.GetPosition(this);

                this.startAngleX = rotateX.Angle;
                this.startAngleY = rotateZ.Angle;
            }
        }

        private void viewport1_MouseUp(object sender, MouseButtonEventArgs e)
        {
            isMouseDown = false;
        }



        private void Checked(object sender, RoutedEventArgs e)
        {
            foreach (var item in tempList)
            {
                MainModel3Dgroup.Children.Add(item);
            }
        }

        private void Unchecked(object sender, RoutedEventArgs e)
        {
            foreach (var item in tempList)
            {
                MainModel3Dgroup.Children.Remove(item);
            }

        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            //switch (e.Key)
            //{
            //    case Key.Up:
            //        CameraPhi += CameraDPhi;
            //        if (CameraPhi > Math.PI / 2.0)
            //            CameraPhi = Math.PI / 2.0;
            //        break;
            //    case Key.Down:
            //        CameraPhi -= CameraDPhi;
            //        if (CameraPhi < -Math.PI / 2.0)
            //            CameraPhi = -Math.PI / 2.0;
            //        break;
            //    case Key.Left:
            //        CameraTheta += CameraDTheta;
            //        break;
            //    case Key.Right:
            //        CameraTheta -= CameraDTheta;
            //        break;
            //    case Key.Add:
            //    case Key.OemPlus:
            //        CameraR -= CameraDR;
            //        if (CameraR < CameraDR) CameraR = CameraDR;
            //        break;
            //    case Key.Subtract:
            //    case Key.OemMinus:
            //        CameraR += CameraDR;
            //        break;
            //}

            //// Update the camera's position.
            //PositionCamera();
        }
        // Position the camera.
        private void PositionCamera()
        {
            // Calculate the camera's position in Cartesian coordinates.
            double y = CameraR * Math.Sin(CameraPhi);
            double hyp = CameraR * Math.Cos(CameraPhi);
            double x = hyp * Math.Cos(CameraTheta);
            double z = hyp * Math.Sin(CameraTheta);
            myPerspectiveCamera.Position = new Point3D(x, y, z);

            // Look toward the origin.
            myPerspectiveCamera.LookDirection = new Vector3D(-x, -y, -z);

            // Set the Up direction.
            myPerspectiveCamera.UpDirection = new Vector3D(0, 1, 0);
        }

    }
}
