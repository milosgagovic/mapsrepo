﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ1.Model
{
    public enum SwitchStatus { Closed, Open};
    public class SwitchEntity : Entity
    {
        public SwitchStatus Status;
        public double X;
        public double Y;
    }
}
