﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PZ1.Model
{
    public class Entity
    {
        public String Id;
        public String Name;
        
        [XmlIgnore]
        public double Latitude;

        [XmlIgnore]
        public double Longitude;

    }
}
