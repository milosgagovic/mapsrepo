﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PZ1.Model
{
    public enum ConductorMaterials { Steel, Acsr, Copper, Other };
    public enum LineTypes { Cable, Overhead};
    public class LineEntity : Entity
    {
        public bool isUnderground;
        public double R;
        public ConductorMaterials ConductorMaterial;
        public LineTypes LineType;
        public double ThermalConstantHeat;
        public String FirstEnd;
        public String SecondEnd;
        public List<Point> Vertices;
    }
}
